NUMBER_OF_VALUES = 13

def add_letter(number):
    return string1[number]

a = 188461
b = 565383
d = 696149
e = 88447
g = 265341
h = 796023
i = 388069
l = 164207
n = 492621
o = 477863
s = 433589
t = 300767
u = 902301

orig_tekst = [696318, 994738, 186634, 937474, 818450, 387210, 184092, 377231, 999091, 293830, 725605, 893049, 394922]
string1 = [a, b, d, e, g, h, i, l, n, 0, s, t, u]
string2 = ['a', 'b', 'd', 'e', 'g', 'h', 'i', 'l', 'n', 'o', 's', 't', 'u']

def write_solution(fp, numb, solution):
    fp.write("word number: ")
    fp.write(str(numb))
    fp.write(" solution: ")
    fp.write(str(solution))
    fp.write('\n')

def limit_value(z):
    while z > 1000000:
        z -= 1000000
    return z

def two_letter_logic(word, numb, fp):
    for x in range(0, NUMBER_OF_VALUES):
        for xx in range(0, NUMBER_OF_VALUES):
            z = string1[x]+string1[xx]
            z = limit_value(z)
            if z == word:
                print ("Gotcha 2!, word " +str(numb) +  str(string2[x]) + str(string2[xx]))
                solution = str(string2[x]) + str(string2[xx])
                write_solution(fp, numb, solution)
    return

def three_letter_logic(word, numb, fp):
    for x in range(0, NUMBER_OF_VALUES):
        for xx in range(0, NUMBER_OF_VALUES):
            for xxx in range(0, NUMBER_OF_VALUES):
                z = string1[x]+string1[xx]+string1[xxx]
                z = limit_value(z)
                if z == word:
                    print ("Gotcha 3!, word " +str(numb) +  str(string2[x]) + str(string2[xx]) + str(string2[xxx]))
                    solution = str(string2[x]) + str(string2[xx] + str(string2[xxx]))
                    write_solution(fp, numb, solution)
    return

def four_letter_logic(word, numb, fp):
    for x in range(0, NUMBER_OF_VALUES):
        for xx in range(0, NUMBER_OF_VALUES):
            for xxx in range(0, NUMBER_OF_VALUES):
                for xxxx in range(0, NUMBER_OF_VALUES):
                    z = string1[x]+string1[xx]+string1[xxx]+string1[xxxx]
                    z = limit_value(z)
                    if z == word:
                        print ("Gotcha!, 4 word " +str(numb))
                        solution = str(string2[x]) + str(string2[xx] + str(string2[xxx])) + str(string2[xxxx])
                        write_solution(fp, numb, solution)
    return

def vife_letter_logic(word, numb, fp):
    for x in range(0, NUMBER_OF_VALUES):
        for xx in range(0, NUMBER_OF_VALUES):
            for xxx in range(0, NUMBER_OF_VALUES):
                for xxxx in range(0, NUMBER_OF_VALUES):
                    for xxxxx in range(0, NUMBER_OF_VALUES):
                        z = string1[x]+string1[xx]+string1[xxx]+string1[xxxx]+string1[xxxxx]
                        z = limit_value(z)
                        if z == word:
                            print ("Gotcha!, 5 word " +str(numb))
                            solution = str(string2[x]) + str(string2[xx] + str(string2[xxx])) + str(string2[xxxx]) + str(string2[xxxx])
                            write_solution(fp, numb, solution)

def six_letter_logic(word, numb, fp):
    for x in range(0, NUMBER_OF_VALUES):
        for xx in range(0, NUMBER_OF_VALUES):
            for xxx in range(0, NUMBER_OF_VALUES):
                for xxxx in range(0, NUMBER_OF_VALUES):
                    for xxxxx in range(0, NUMBER_OF_VALUES):
                        for xxxxxx in range(0, NUMBER_OF_VALUES):
                            z = string1[x]+string1[xx]+string1[xxx]+string1[xxxx]+string1[xxxxx]+string1[xxxxxx]
                            z = limit_value(z)
                            if z == word:
                                print ("Gotcha!, 6 word " +str(numb))
                                solution = str(string2[x]) + str(string2[xx] + str(string2[xxx])) + str(string2[xxxx]) + str(string2[xxxxx]) + str(string2[xxxxxx])
                                write_solution(fp, numb, solution)

def seven_letter_logic(word, numb, fp):
    for x in range(0, NUMBER_OF_VALUES):
        for xx in range(0, NUMBER_OF_VALUES):
            for xxx in range(0, NUMBER_OF_VALUES):
                for xxxx in range(0, NUMBER_OF_VALUES):
                    for xxxxx in range(0, NUMBER_OF_VALUES):
                        for xxxxxx in range(0, NUMBER_OF_VALUES):
                            for xxxxxxx in range(0, NUMBER_OF_VALUES):
                                z = string1[x]+string1[xx]+string1[xxx]+string1[xxxx]+string1[xxxxx]+string1[xxxxxx]+string1[xxxxxxx]
                                z = limit_value(z)
                                if z == word:
                                    print ("Gotcha!, 7 word " +str(numb))
                                    solution = str(string2[x]) + str(string2[xx] + str(string2[xxx])) + str(string2[xxxx]) + str(string2[xxxxx]) + str(string2[xxxxxx]) + str(string2[xxxxxxx])
                                    write_solution(fp, numb, solution)


fp =open("outcome2.", 'w')
for words in range(0, NUMBER_OF_VALUES):

    two_letter_logic(orig_tekst[words], words, fp)
    three_letter_logic(orig_tekst[words], words, fp)
    four_letter_logic(orig_tekst[words], words, fp)
    vife_letter_logic(orig_tekst[words], words, fp)
    six_letter_logic(orig_tekst[words], words, fp)
    #seven_letter_logic(orig_tekst[words], words)

fp.close()
print("finished script")


#print(str(solution))



